#pragma once
#include <string>
#include <functional>
#include <vector>

#include "json.hpp"

enum status_codes{
	NotFound,
	OK,
	NotImplemented,
};


namespace utility{
  typedef std::string string_t;
}

namespace web{
}


namespace cfx{
  struct uri{
	std::string value;
  uri(const std::string& _value):value(_value){}
	std::string scheme() const{
	  return "";
	}
	std::string host() const{
	  return "host_auto_ipv4";
	}
	uint16_t port() const{
	  return 0;
	}
	std::string path() const{
	  return "path";
	}
	std::string to_string() const{
	  return "no implemented";
	}
	static std::string decode(std::string const &  path){
	  return "no implemented";
	}
	static std::vector<std::string> split_path(std::string const&  path){
	  return std::vector<std::string>();
	}
  };
  struct uri_builder{
	std::string scheme, path, host;
	uint16_t port;
	void set_scheme(std::string const & scheme){
	  this->scheme = scheme;
	}
	void set_host(std::string const & host){
	  this->host = host;
	  
	}
	void set_port(uint16_t port){
	  this->port = port;
	}
	void set_path(std::string const& path){
	  this->path = path;
	}
	uri to_uri() const{
	  return uri("not implemented");
	}
  };

}
namespace pplx{
  template<typename T>
	struct task{
	  T wait(){
	  }
	};
  
}
namespace http{
  
  struct http_request{
	void reply(status_codes code, json::value value){
	  
	}
	void reply(status_codes code){
	  // not implementd
	}
	cfx::uri relative_uri() const {
	  return cfx::uri("not implement");
	}
	
  };
  typedef std::string method;
  namespace methods{
	const std::string GET = "GET";
	const std::string PUT = "GET";	
	const std::string POST = "GET";
	const std::string DEL = "GET";
	const std::string PATCH = "GET";
	const std::string OPTIONS = "GET";
	const std::string HEAD = "HEAD";
	const std::string CONNECT = "HEAD";
	const std::string MERGE = "HEAD";
	const std::string TRCE = "HEAD";
	//PUT,
	//POST,
	//DEL,
	//PATCH,
  };
namespace experimental{

  
  namespace listener{
	struct http_listener{
	  cfx::uri _uri;
	  
	http_listener():_uri("no implemented"){}
	  http_listener(const cfx::uri & i):_uri(i){}
	  cfx::uri uri() const {
		return _uri;
	  }
	  void support(http::method method,
				   std::function<void(http_request)> f){
	  }
	  pplx::task<void> close(){
	  }
	  pplx::task<void> open(){
		return pplx::task<void>();
	  }
	};
	
  };
};
};
